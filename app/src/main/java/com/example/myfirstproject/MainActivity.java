package com.example.myfirstproject;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void convertButtonPressed(View view) {
        // 0. Debug code to check that button is working
//        Toast t = Toast.makeText(this, "CONVERT BUTTON IS WORKING!", Toast.LENGTH_SHORT);
//        t.show();

        // 1. Get the amount from the text box
        // @TODO:  Change editText2 to match YOUR id
        EditText amountTextBox = (EditText) findViewById(R.id.editText2);
        String userInput = amountTextBox.getText().toString();
        double amount = Double.parseDouble(userInput);

        // 2. Do the math
        double EXCHANGE_RATE = 0.75;
        double result = amount * EXCHANGE_RATE;

        Toast t = Toast.makeText(this, "Converted amount: " + result, Toast.LENGTH_SHORT);
        t.show();


        // 3. Display the result on the screen
        // @TODO:  Change textView5 to match YOUR id
        TextView resultsLabel = (TextView) findViewById(R.id.textView5);
        resultsLabel.setText("Converted amount is: " + result);

    }





}
